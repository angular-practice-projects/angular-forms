import { Component } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'course-form',
  templateUrl: './course-form.component.html',
  styleUrls: ['./course-form.component.css']
})
export class CourseFormComponent {

  form = new FormGroup({
    topics:new FormArray([]),
    input: new FormControl('',Validators.required)
  })

  addTopic(topic: HTMLInputElement){
    if(this.form.valid === true){
      this.topics.push(new FormControl(topic.value));
      topic.value = '';
    }
  }

  removeTopic(topic: FormControl){
    let index = this.topics.controls.indexOf(topic);
    this.topics.removeAt(index);
  }
  updateTopic(topic: FormControl){
    let index = this.topics.controls.indexOf(topic);
    let text = prompt('Modify Task : ',topic.value);
    if(text != null && text.trim() !== ''){
      topic.reset(text);
    }
  }

  get topics(){
    return this.form.get('topics') as FormArray
  }

}
