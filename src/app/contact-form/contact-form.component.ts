import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'contact-form',
  templateUrl: './contact-form.component.html',
  styleUrls: ['./contact-form.component.css']
})
export class ContactFormComponent{

  contactMethods=[
    {id:1, name:'Email'},
    {id:2, name:'Whatsapp'},
    {id:3, name:'Contact'},
  ]

  log(msg){
    console.log('came');
    console.log(msg);
  }

  submit(form){
    console.log(form)
  }
}
